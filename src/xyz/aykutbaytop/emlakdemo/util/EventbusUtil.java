package xyz.aykutbaytop.emlakdemo.util;

import com.google.common.eventbus.EventBus;

public class EventbusUtil {
	private static EventBus eventBus = null;
	private static Object lock = new Object();

	public static EventBus getEventBus(){
		if(eventBus == null){
			synchronized (lock) {
				if (eventBus == null){
					eventBus = new EventBus();
				}
			}
		}
		return eventBus;
	}
}
