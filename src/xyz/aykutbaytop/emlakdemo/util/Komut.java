package xyz.aykutbaytop.emlakdemo.util;

import xyz.aykutbaytop.emlakdemo.enums.KomutTuru;

public class Komut {
	public KomutTuru turu;
	public Object obj;
	
	public Komut(KomutTuru id, Object obj){
		this.turu=id;
		this.obj=obj;
	}

	public Komut(KomutTuru id){
		this.turu=id;
		this.obj=null;
	}
}
