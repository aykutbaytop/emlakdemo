package xyz.aykutbaytop.emlakdemo.util;

import java.nio.ByteBuffer;
import java.util.UUID;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

@Converter(autoApply = true)
public class UUIDAttributeConverter implements AttributeConverter<UUID, String>{

	@Override
	public String convertToDatabaseColumn(UUID value) {
		if (value == null) return null;
	    return value.toString().replaceAll("-", "");	
	}

	@Override
	public UUID convertToEntityAttribute(String value) {
		if (value == null) return null;
		byte[] data;
		try {
			data = Hex.decodeHex(value.toCharArray());
			return new UUID(ByteBuffer.wrap(data, 0, 8).getLong(), ByteBuffer.wrap(data, 8, 8).getLong());
		} catch (DecoderException e) {
			return null;
		}
	}

}
