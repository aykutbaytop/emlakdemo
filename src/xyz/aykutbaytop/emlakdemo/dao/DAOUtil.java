package xyz.aykutbaytop.emlakdemo.dao;

import java.util.HashMap;
import java.util.Map;

import xyz.aykutbaytop.emlakdemo.model.Emlak;
import xyz.aykutbaytop.emlakdemo.model.Isletme;
import xyz.aykutbaytop.emlakdemo.model.Musteri;

public class DAOUtil {
	private static Object lock = new Object();
	private static Map<Class<?>, EmlakDAOInterface<?>> map = new HashMap<Class<?>, EmlakDAOInterface<?>>();

	public static <T> EmlakDAOInterface<T> getDaoFor(Class<T> cls){
		
		if (map.get(cls) == null) {
			synchronized (lock) {
				if (map.get(cls) == null){
					EmlakDAOInterface<T> dao = null;
					
					if (cls.equals(Emlak.class))
						dao = (EmlakDAOInterface<T>) new EmlakDAO();
					
					else if (cls.equals(Isletme.class))
						dao = (EmlakDAOInterface<T>) new IsletmeDAO();
					
					else if (cls.equals(Musteri.class))
						dao = (EmlakDAOInterface<T>) new MusteriDAO();
					if (dao != null)
						map.put(cls, dao);
					return dao;
				}
				else 
					return (EmlakDAOInterface<T>)map.get(cls);
			}
		}
		else 
			return (EmlakDAOInterface<T>)map.get(cls);
	}
}
