package xyz.aykutbaytop.emlakdemo.dao;

import java.util.List;
import java.util.Optional;

import javax.persistence.Query;

import xyz.aykutbaytop.emlakdemo.model.Isletme;

public class IsletmeDAO implements EmlakDAOInterface<Isletme> {
	public Isletme getIsletmeInData(){
		Isletme isl =null;
		Query query = em.createQuery ("select i from ISLETME i where i.id =:ID", Integer.class).setParameter("ID", 1);
		Optional first = query.getResultList().stream().findFirst();
		if(first.isPresent())
			isl = (Isletme) query.getSingleResult();
		else
			isl = new Isletme();
		return isl;
	}
	@Override
	public void add(Isletme value) {
		em.getTransaction().begin();
		em.persist(value);
		em.getTransaction().commit();
		
	}

	@Override
	public List<Isletme> list() {
		return null;
	}
	@Override
	public void remove(Isletme value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Isletme value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void refresh(Isletme value) {
		// TODO Auto-generated method stub
		
	}

	
}	
