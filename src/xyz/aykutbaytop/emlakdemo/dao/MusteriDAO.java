package xyz.aykutbaytop.emlakdemo.dao;


import java.util.List;

import javax.persistence.Query;

import xyz.aykutbaytop.emlakdemo.model.Isletme;
import xyz.aykutbaytop.emlakdemo.model.Musteri;

public class MusteriDAO implements EmlakDAOInterface<Musteri> {

	@Override
	public void add(Musteri value) {
		em.getTransaction().begin();
		em.persist(value);
		em.getTransaction().commit();	
	}
	public List<Musteri>searchMusteri(String value){

		if(!"".equals(value)){
			String upper = value.toUpperCase();
			Query query = em.createQuery ("select m from MUSTERI m where m.ad LIKE :ISIM", String.class).setParameter("ISIM", upper+"%");
			List<Musteri> list = query.getResultList();
			return list;
		}
			
		return null;
	}
	@Override
	public List<Musteri> list() {
		Query query = em.createQuery ("select m from MUSTERI m", Musteri.class);
		List<Musteri> list = query.getResultList();
		return list;
	}

	@Override
	public void remove(Musteri value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Musteri value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void refresh(Musteri value) {
		// TODO Auto-generated method stub
		
	}
}	
