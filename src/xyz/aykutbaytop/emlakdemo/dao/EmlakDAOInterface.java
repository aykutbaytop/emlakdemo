package xyz.aykutbaytop.emlakdemo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public interface EmlakDAOInterface<T> {
	static final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("pers");
	static final EntityManager em = entityManagerFactory.createEntityManager();

	void add(T value);
	List<T> list();
	void remove(T value);
	void update(T value);
	void refresh(T value);
}
