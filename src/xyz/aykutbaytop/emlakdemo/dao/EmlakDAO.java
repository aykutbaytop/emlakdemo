package xyz.aykutbaytop.emlakdemo.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Query;

import xyz.aykutbaytop.emlakdemo.enums.EmlakDurumu;
import xyz.aykutbaytop.emlakdemo.enums.EmlakTuru;
import xyz.aykutbaytop.emlakdemo.enums.IsitmaTuru;
import xyz.aykutbaytop.emlakdemo.model.Emlak;
import xyz.aykutbaytop.emlakdemo.model.Musteri;

public class EmlakDAO implements EmlakDAOInterface<Emlak> {

	@Override
	public void add(Emlak value) {
		em.getTransaction().begin();
		em.persist(value);
		em.getTransaction().commit();	
	}

	@Override
	public List<Emlak> list() {
		Query q = em.createQuery("select e from EMLAK e WHERE e.emlakDurumu = :emlakDurumu", Emlak.class);
		q.setParameter("emlakDurumu", EmlakDurumu.AKTIF);
		List<Emlak> emlakList = q.getResultList();
		return emlakList;
	}
	public List<Emlak> emlakListByTuru(EmlakTuru paramTuru,IsitmaTuru paramIsitma,String paramOdaSay,String paramSalonSay){
		System.out.println(paramTuru +" - "+ paramIsitma);
		StringBuilder jpql = new StringBuilder("select e from EMLAK e");
		
		List<String> where = new ArrayList<>();
		
				
		if (paramTuru != EmlakTuru.TANIMSIZ) {
			where.add("e.turu = :TURU");
		}
		
		if (paramIsitma!=IsitmaTuru.TANIMSIZ) {
			where.add("e.isitmaTuru = :ISITMA_TURU");
		}
		
		if (!paramOdaSay.equals("")) {
			where.add("e.odaSayisi = :ODA_SAYISI");
		}
		
		if (!paramSalonSay.equals("")) {
			where.add("e.salonSayisi = :SALON_SAYISI");
		}
		
		if (where.size()>0) 
			jpql.append(" where ").append(where.stream().collect(Collectors.joining(" and ")));
		
		Query query = em.createQuery(jpql.toString(), Musteri.class);

		if (paramTuru != EmlakTuru.TANIMSIZ) {
			query.setParameter("TURU", paramTuru);
		}
		
		if  (paramIsitma!=IsitmaTuru.TANIMSIZ) {
			query.setParameter("ISITMA_TURU", paramIsitma);
		}
		if (!paramOdaSay.equals("")) {
			query.setParameter("ODA_SAYISI", paramOdaSay);
		}
		
		if (!paramSalonSay.equals("")) {
			query.setParameter("SALON_SAYISI", paramSalonSay);
		}
		List<Emlak> list = query.getResultList();
		return list;	
		
	}

	@Override
	public void remove(Emlak value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Emlak value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void refresh(Emlak value) {
		// TODO Auto-generated method stub
		
	}
	


}	
