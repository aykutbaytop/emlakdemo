package xyz.aykutbaytop.emlakdemo.controller;

import java.io.IOException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import xyz.aykutbaytop.emlakdemo.dao.DAOUtil;
import xyz.aykutbaytop.emlakdemo.model.Musteri;

public class MusteriDialog extends Dialog<Musteri> {
	ButtonType buttonTypeOk = new ButtonType("Se�", ButtonData.OK_DONE);
	ButtonType buttonTypeCancel = new ButtonType("�ptal", ButtonData.CANCEL_CLOSE);
	

	private ObservableList<Musteri> musteriObs = FXCollections.observableArrayList();
	public MusteriDialog() {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("MusteriDialog.fxml"));
        loader.setController(this);
        try {
			this.getDialogPane().setContent((BorderPane)loader.load());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        getDialogPane().getButtonTypes().setAll(buttonTypeOk, buttonTypeCancel);
        
        setResultConverter(dialogButton -> {
            if (dialogButton == buttonTypeOk) {
                return musteriTablosu.getSelectionModel().getSelectedItem();
            }
            return null;
        });        
	}
	@FXML private TableView<Musteri> musteriTablosu;
	@FXML private TableColumn<Musteri, String> isimCol;
	@FXML private TableColumn<Musteri, String> soyisimCol;
	@FXML private TableColumn<Musteri, String> snoCol;
	@FXML private TextField aramaTxt;

	@FXML 
	private void initialize(){
		musteriObs.addAll(DAOUtil.getDaoFor(Musteri.class).list());
		isimCol.setCellValueFactory((cellData -> cellData.getValue().adProperty()));
		soyisimCol.setCellValueFactory(e->e.getValue().soyadProperty());
		musteriTablosu.setItems(musteriObs);
		aramaTxt.textProperty().addListener((observable, oldValue, newValue) -> {
			if (oldValue != null && (newValue.length() < oldValue.length())) {
				musteriTablosu.setItems(musteriObs);
			}
			String value = newValue.toLowerCase();
			ObservableList<Musteri> subentries = FXCollections.observableArrayList();

			long count = musteriTablosu.getColumns().stream().count();

			for (int i = 0; i < musteriTablosu.getItems().size(); i++) {
				for (int j = 0; j < count; j++) {
					String entry = "" + musteriTablosu.getColumns().get(j).getCellData(i);
					if (entry.toLowerCase().contains(value)) {
						subentries.add(musteriTablosu.getItems().get(i));
						break;
					}
				}
			}
			musteriTablosu.setItems(subentries);
		});

		musteriTablosu.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event){
				if(event.getClickCount() ==2)
			        setResult(musteriTablosu.getSelectionModel().getSelectedItem());
			}
		});
		musteriTablosu.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				if(event.getCode().equals(KeyCode.ENTER))
					setResult(musteriTablosu.getSelectionModel().getSelectedItem());
			}
		});
	}
}
