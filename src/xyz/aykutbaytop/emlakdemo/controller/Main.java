package xyz.aykutbaytop.emlakdemo.controller;

import java.io.IOException;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application{
	private Stage priStage;
	private BorderPane root;
	private TabPane tp;
	

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.priStage = primaryStage;
		priStage.setTitle("EMLAK DEMO");
		priStage.setMaximized(true);
		rootMenu();
		show();
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);
            }
        });		
	}
    public void rootMenu() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("RootLayout.fxml"));
            root = (BorderPane) loader.load();
            Scene scene = new Scene(root);
            RootLayoutController root = loader.getController();
            tp = root.getMainTabPane();
            priStage.setScene(scene);
            priStage.show();
           
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    public void show() {
        try {
        	FXMLLoader loader = new FXMLLoader(this.getClass().getResource("MainTab.fxml"));
    		Tab tb2 = (Tab) loader.load();
    		tb2.setText("Anasayfa");
    		tp.getTabs().add(tb2);
    		tp.getSelectionModel().select(tb2);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public static void main (String[] args) {
		launch(args);
	}
}
