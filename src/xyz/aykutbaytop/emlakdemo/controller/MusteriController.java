package xyz.aykutbaytop.emlakdemo.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import xyz.aykutbaytop.emlakdemo.dao.DAOUtil;
import xyz.aykutbaytop.emlakdemo.dao.MusteriDAO;
import xyz.aykutbaytop.emlakdemo.model.Musteri;

public class MusteriController implements Initializable {
	@FXML
	private Tab musteriTab;
	@FXML
	private TextField ePostaTxt;
	@FXML
	private TextField adTxt;
	@FXML
	private TextField soyadTxt;
	@FXML
	private TextField evTelefonTxt;
	@FXML
	private TextField cepTelefonTxt;
	@FXML
	private TextArea adresTxt;

	private Musteri musteri = null;

	public MusteriController(Musteri value) {
		if (value == null) {
			this.musteri = new Musteri();
		}
		else{
			this.musteri = value;
		}
	}
	private MusteriDAO musteriDAO = (MusteriDAO) DAOUtil.getDaoFor(Musteri.class);

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		musteriTab.setText("Yeni M��teri");

		if (musteri.getId() != null) 
			musteriTab.setText(musteri.getAd() + " " + musteri.getSoyad());
		
		adTxt.textProperty().bindBidirectional(musteri.adProperty());
		soyadTxt.textProperty().bindBidirectional(musteri.soyadProperty());
		ePostaTxt.textProperty().bindBidirectional(musteri.ePostaProperty());
		evTelefonTxt.textProperty().bindBidirectional(musteri.evTelefonuProperty());
		cepTelefonTxt.textProperty().bindBidirectional(musteri.cepTelefonuProperty());
		adresTxt.textProperty().bindBidirectional(musteri.adresProperty());
		
	}
	private void kaydet() {
		musteriDAO.add(musteri);
		
	}
	@FXML
	private void kaydetBtn() {
		kaydet();
	}

	@FXML
	public void kaydetKapatBtn() {
		kaydet();
		musteriTab.getTabPane().getTabs().remove(musteriTab);
	}
	@FXML
	private void iptalBtn(ActionEvent e) {
		musteriTab.getTabPane().getTabs().remove(musteriTab);
	}
}
