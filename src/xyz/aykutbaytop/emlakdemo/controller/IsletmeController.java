package xyz.aykutbaytop.emlakdemo.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import xyz.aykutbaytop.emlakdemo.dao.DAOUtil;
import xyz.aykutbaytop.emlakdemo.dao.IsletmeDAO;
import xyz.aykutbaytop.emlakdemo.model.Isletme;

public class IsletmeController implements Initializable {
	@FXML
	private Tab isletmeTab;
	@FXML
	private TextField ePostaTxt;
	@FXML
	private TextField adTxt;
	@FXML
	private TextField yetkiliTxt;
	@FXML
	private TextField telefonTxt;
	@FXML
	private TextField faxTxt;
	@FXML
	private TextArea adresTxt;

	private Isletme isletme = null;

	public IsletmeController(Isletme value) {
		if (value == null)
			this.isletme = new Isletme();
		else
			this.isletme = value;
	}
	private IsletmeDAO isletmeDAO = (IsletmeDAO) DAOUtil.getDaoFor(Isletme.class);

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		adTxt.setText(isletme.getAd());
		yetkiliTxt.setText(isletme.getYetkili());
		telefonTxt.setText(isletme.getTelefonu());
		faxTxt.setText(isletme.getFax());
		ePostaTxt.setText(isletme.getEposta());
		adresTxt.setText(isletme.getAdres());
		
	}
	private void kaydet() {
		isletme.setAd(adTxt.getText());
		isletme.setAdres(adresTxt.getText());
		isletme.setEposta(ePostaTxt.getText());
		isletme.setFax(faxTxt.getText());
		isletme.setId(1);
		isletme.setTelefonu(telefonTxt.getText());
		isletme.setYetkili(yetkiliTxt.getText());
		isletmeDAO.add(isletme);
		
	}
	@FXML
	private void kaydetBtn() {
		kaydet();
	}

	@FXML
	public void kaydetKapatBtn() {
		kaydet();
		isletmeTab.getTabPane().getTabs().remove(isletmeTab);
	}
	@FXML
	private void iptalBtn(ActionEvent e) {
		isletmeTab.getTabPane().getTabs().remove(isletmeTab);
	}
}
