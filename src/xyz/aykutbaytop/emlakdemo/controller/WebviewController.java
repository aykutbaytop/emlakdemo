package xyz.aykutbaytop.emlakdemo.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import xyz.aykutbaytop.emlakdemo.dao.DAOUtil;
import xyz.aykutbaytop.emlakdemo.dao.MusteriDAO;
import xyz.aykutbaytop.emlakdemo.model.Musteri;

public class WebviewController implements Initializable {
	@FXML
	private Tab webviewTab;
	@FXML
	private VBox vbox;

	private WebView webview = null;

	public WebviewController(WebView value) {
			this.webview = value;
	}
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		webviewTab.setText("Emlak D�k�m�");
		vbox.getChildren().setAll(webview);
		
	}
	@FXML
	private void yazdirBtn(ActionEvent e) {
	    Printer printer = Printer.getDefaultPrinter();

		PrinterJob printJob = PrinterJob.createPrinterJob(printer);
		if(printJob!=null){
	        webview.getEngine().print(printJob);
	        printJob.endJob();	
		}
		else
			System.out.println("printer hatas� olu�tu");
	}

	@FXML
	private void iptalBtn(ActionEvent e) {
		webviewTab.getTabPane().getTabs().remove(webviewTab);
	}
}
