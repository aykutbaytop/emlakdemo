package xyz.aykutbaytop.emlakdemo.controller;

import java.net.URL;
import java.util.ResourceBundle;

import com.google.common.eventbus.Subscribe;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebEngineBuilder;
import javafx.scene.web.WebView;
import xyz.aykutbaytop.emlakdemo.dao.DAOUtil;
import xyz.aykutbaytop.emlakdemo.dao.EmlakDAO;
import xyz.aykutbaytop.emlakdemo.dao.IsletmeDAO;
import xyz.aykutbaytop.emlakdemo.enums.EmlakTuru;
import xyz.aykutbaytop.emlakdemo.enums.IsitmaTuru;
import xyz.aykutbaytop.emlakdemo.enums.KomutTuru;
import xyz.aykutbaytop.emlakdemo.model.Emlak;
import xyz.aykutbaytop.emlakdemo.model.Isletme;
import xyz.aykutbaytop.emlakdemo.util.EventbusUtil;
import xyz.aykutbaytop.emlakdemo.util.Komut;

public class MainController implements Initializable {
	@FXML 
	private Tab mainTab;
	
	@FXML 
	private TableView<Emlak>emlakTable;
	@FXML 
	private TableColumn<Emlak, EmlakTuru>turuCol;
	@FXML 
	private TableColumn<Emlak, String>metreKareCol;
	@FXML 
	private TableColumn<Emlak, String>odaSayisiToplamCol;
	@FXML 
	private TableColumn<Emlak, String>katCol;
	@FXML 
	private TableColumn<Emlak, String>ilCol;
	@FXML 
	private TableColumn<Emlak, String>ilceCol;
	@FXML 
	private TableColumn<Emlak, IsitmaTuru>isitmaTuruCol;
	
	
	@FXML 
	private TextField odaSayisiTxt;
	@FXML 
	private TextField salonSayisiTxt;
	@FXML 
	private TextField metreKare2Txt;
	@FXML 
	private ComboBox<EmlakTuru>emlakTuruCmb;
	@FXML 
	private ComboBox<IsitmaTuru>isitmaTuruCmb;

	
	private EmlakDAO emlakDAO = (EmlakDAO) DAOUtil.getDaoFor(Emlak.class);
	private IsletmeDAO isletmeDAO = (IsletmeDAO) DAOUtil.getDaoFor(Isletme.class);
	private ObservableList<Emlak> emlakObs;
	
	private final ObservableList<EmlakTuru>emlakTurObs = FXCollections.observableArrayList(EmlakTuru.values());
	private final ObservableList<IsitmaTuru>isitmaTurObs = FXCollections.observableArrayList(IsitmaTuru.values());
	@Subscribe
	public void event(Emlak emlak){
		if (emlakObs.contains(emlak)){
			emlakObs.remove(emlak);
		}
		emlakObs.add(emlak);
		emlakTable.setItems(emlakObs);
	}
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		EventbusUtil.getEventBus().register(this);

		emlakTuruCmb.setItems(emlakTurObs);
		isitmaTuruCmb.setItems(isitmaTurObs);
		emlakTuruCmb.setValue(EmlakTuru.TANIMSIZ);
		isitmaTuruCmb.setValue(IsitmaTuru.TANIMSIZ);
		emlakObs = FXCollections.observableList(emlakDAO.list());
		if(emlakObs.size()>0 && emlakObs != null){
			emlakTable.setItems(emlakObs);
		}
			
		else
			System.out.println("liste bo�");
		turuCol.setCellValueFactory(e->e.getValue().turuProperty());
		metreKareCol.setCellValueFactory(e->e.getValue().metreKareProperty());
		odaSayisiToplamCol.setCellValueFactory(e->e.getValue().odaSayisiProperty());
		katCol.setCellValueFactory(e->e.getValue().katNoProperty());
		ilCol.setCellValueFactory(e->e.getValue().ilProperty());
		ilceCol.setCellValueFactory(e->e.getValue().ilceProperty());
		isitmaTuruCol.setCellValueFactory(e->e.getValue().isitmaTuruProperty());
	}
	@FXML
	private void araBtn(){
		emlakObs.clear();
		emlakObs.addAll(emlakDAO.emlakListByTuru(emlakTuruCmb.getValue(),isitmaTuruCmb.getValue(),odaSayisiTxt.getText(),salonSayisiTxt.getText()));
	}
	@FXML
	private void yazdirBtn(){
		Emlak emlak = emlakTable.getSelectionModel().getSelectedItem();
		if(emlak==null){
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setHeaderText("L�tfen Tablodan Emlak Se�imi Yap�n�z.");
			alert.show();
		}
		else{
			Isletme isletme = isletmeDAO.getIsletmeInData();		
			WebView webView = new WebView();
			WebEngine engine = webView.getEngine();
			StringBuilder html = new StringBuilder();
			html.append("<html><head><title>Title</title></head><body>");
			html.append("<table><tr><td><h4>");
			html.append(emlak.getIslemTuru() + " " + emlak.getTuru());
			html.append("</h4></td></tr>"
					+ ""
					+ "<tr><td>B�LG�LER�</td></tr>"
					+ "<tr>"
					+ "<td>ODA/SALON SAYISI :" + emlak.getOdaSayisi()+"+"+emlak.getSalonSayisi()+ "</td>"
					+ "<td>" + "KAT :" + emlak.getKatNo()+ "</td>"
					+ "</tr>"
					+ "<tr>"
					+ "<td>ISITMA :" + emlak.getIsitmaTuru()+"</td>"
					+ "<td>" + "METRE KARE :" + emlak.getMetreKare()+ "</td>"
					+ "</tr>"
					+ "<tr>"
					+ "<td>�L :" + emlak.getIl()+"</td>"
					+ "<td>�L�E :" + emlak.getIlce()+"</td>"
					+ "</tr>"
					+ "<tr>"
					+ "<td>ADRES :" + emlak.getAdres()+"</td>"
					+ "</tr>"
					
					
					
					+ "<tr>"
					
					+ "<td>" + isletme.getAd()+"EMLAK</td>"
					+ "<td></td>"
					+ "<td></td>"
					+ "</tr>"
					
					+ "<tr>"
					+ "<td>TELEFON :" + isletme.getTelefonu()+"</td>"
					+ "<td>EPOSTA :" + isletme.getEposta()+"</td>"
					+ "<td></td>"
					+ "</tr>"
					+ "<tr>"
					+ "<td>FAX :" + isletme.getFax()+"</td>"
					+ "<td>ADRES :" + isletme.getAdres()+"</td>"
					+ "<td></td>"

					+ "</tr>"
															
					+ "</table></body></html>");
			engine.loadContent(html.toString());
			EventbusUtil.getEventBus().post(new Komut(KomutTuru.WEBVIEW,webView));
		}			
	}
}