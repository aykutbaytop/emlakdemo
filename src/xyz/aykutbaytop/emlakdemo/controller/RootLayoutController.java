package xyz.aykutbaytop.emlakdemo.controller;

import java.io.IOException;

import com.google.common.eventbus.Subscribe;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.web.WebView;
import xyz.aykutbaytop.emlakdemo.dao.DAOUtil;
import xyz.aykutbaytop.emlakdemo.dao.IsletmeDAO;
import xyz.aykutbaytop.emlakdemo.enums.KomutTuru;
import xyz.aykutbaytop.emlakdemo.model.Emlak;
import xyz.aykutbaytop.emlakdemo.model.Isletme;
import xyz.aykutbaytop.emlakdemo.model.Musteri;
import xyz.aykutbaytop.emlakdemo.util.CheckException;
import xyz.aykutbaytop.emlakdemo.util.EventbusUtil;
import xyz.aykutbaytop.emlakdemo.util.Komut;

public class RootLayoutController {

	@FXML
	TabPane mainTabPane;
	public TabPane getMainTabPane() {
		return mainTabPane;
	}
	private IsletmeDAO islDAO = (IsletmeDAO) DAOUtil.getDaoFor(Isletme.class);
    @FXML
    public void initialize() {
    	EventbusUtil.getEventBus().register(this);
    }
	
	@FXML 
	private void yeniMusteriBtn() throws CheckException{
		EventbusUtil.getEventBus().post(new Komut(KomutTuru.MUSTERI));
	}
	@FXML 
	private void musteriListesiBtn(){
		EventbusUtil.getEventBus().post(new Komut(KomutTuru.MUSTERI_LIST));
	}
	@FXML 
	private void emlakListesiBtn(){
		EventbusUtil.getEventBus().post(new Komut(KomutTuru.EMLAK_LIST));
	}
	@FXML 
	private void yeniEmlakIsyeriBtn(){
		EventbusUtil.getEventBus().post(new Komut(KomutTuru.EMLAK));
	}
	@FXML 
	private void yeniEmlakKonutBtn(){
		EventbusUtil.getEventBus().post(new Komut(KomutTuru.EMLAK));
	}
	@FXML 
	private void yeniIslemArsaBtn(){
		EventbusUtil.getEventBus().post(new Komut(KomutTuru.EMLAK));
	}
	@FXML 
	private void bilgilerimBtn(){
		Isletme i = islDAO.getIsletmeInData();
		EventbusUtil.getEventBus().post(new Komut(KomutTuru.ISLETME,i));
	}
	@Subscribe
	public void subscribe(Komut komut) {
		switch (komut.turu) {
			case MUSTERI:
				musteriTab((Musteri) komut.obj);
				break;
			case MUSTERI_LIST:
				musteriListesiTab();
				break;
			case ISLETME:
				isletmeTab((Isletme)komut.obj);
				break;
			case EMLAK:
				emlakTab((Emlak) komut.obj);
				break;
			case WEBVIEW:
				webviewTab((WebView) komut.obj);
				break;
		}
	}
	private void musteriTab(Musteri value) {
		MusteriController controller = new MusteriController(value);
		FXMLLoader loader = new FXMLLoader(MainController.class.getResource("MusteriTab.fxml"), null, null,
				type -> controller);
		try {
			Tab musteriTab;
			musteriTab = (Tab) loader.load();
			mainTabPane.getTabs().add(musteriTab);
			mainTabPane.getSelectionModel().select(musteriTab);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void musteriListesiTab() {
		FXMLLoader loader = new FXMLLoader(MusteriListesiController.class.getResource("MusteriListesiTab.fxml"));
		try {
			Tab musteriListesiTab;
			musteriListesiTab = (Tab) loader.load();
			mainTabPane.getTabs().add(musteriListesiTab);
			mainTabPane.getSelectionModel().select(musteriListesiTab);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void emlakTab(Emlak value) {
		EmlakController controller = new EmlakController(value);
		FXMLLoader loader = new FXMLLoader(EmlakController.class.getResource("EmlakTab.fxml"), null, null,
				type -> controller);
		try {
			Tab emlakTab;
			emlakTab = (Tab) loader.load();
			mainTabPane.getTabs().add(emlakTab);
			mainTabPane.getSelectionModel().select(emlakTab);
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	private void webviewTab(WebView value) {
		WebviewController controller = new WebviewController(value);
		FXMLLoader loader = new FXMLLoader(WebviewController.class.getResource("WebviewTab.fxml"), null, null,
				type -> controller);
		try {
			Tab webviewTab;
			webviewTab = (Tab) loader.load();
			mainTabPane.getTabs().add(webviewTab);
			mainTabPane.getSelectionModel().select(webviewTab);
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	private void isletmeTab(Isletme value) {
		IsletmeController controller = new IsletmeController(value);
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("IsletmeTab.fxml"), null, null,
				type -> controller);
		try {
			Tab isletmeTab;
			isletmeTab = (Tab) loader.load();
			mainTabPane.getTabs().add(isletmeTab);
			mainTabPane.getSelectionModel().select(isletmeTab);
		} catch (IOException e) {
			e.printStackTrace();
		}			
	}	

}
