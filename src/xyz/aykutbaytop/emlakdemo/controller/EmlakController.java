package xyz.aykutbaytop.emlakdemo.controller;

import java.time.Duration;
import java.util.List;

import org.reactfx.EventStreams;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.stage.Popup;
import xyz.aykutbaytop.emlakdemo.dao.DAOUtil;
import xyz.aykutbaytop.emlakdemo.dao.MusteriDAO;
import xyz.aykutbaytop.emlakdemo.enums.EmlakDurumu;
import xyz.aykutbaytop.emlakdemo.enums.EmlakTuru;
import xyz.aykutbaytop.emlakdemo.enums.IsitmaTuru;
import xyz.aykutbaytop.emlakdemo.enums.IslemTuru;
import xyz.aykutbaytop.emlakdemo.model.Emlak;
import xyz.aykutbaytop.emlakdemo.model.Musteri;
import xyz.aykutbaytop.emlakdemo.util.EventbusUtil;

public class EmlakController {
	@FXML private Tab emlakTab;
	@FXML private TextField musteriAraTxt;
	@FXML private ComboBox<EmlakTuru>emlakTuruCmb;
	@FXML private TextField metreKareTxt;
	@FXML private TextField odaSayTxt;
	@FXML private TextField salonSayTxt;
	@FXML private TextField katNoTxt;
	@FXML private ComboBox<IsitmaTuru>isitmaTuruCmb;
	@FXML private ComboBox<IslemTuru>islemTuruCmb;
	@FXML private TextField ilTxt;
	@FXML private TextField ilceTxt;
	@FXML private TextArea adresTxt;
	
	private Emlak emlak;
	private Musteri musteri;
	public EmlakController(Emlak value){
		this.emlak = value;	
		if(value==null){
			emlak = new Emlak();
			emlak.setMusteri(musteri);
		}
			
	}
	private ObservableList<Musteri>musteriSearchList;
	private ListView<Musteri> searchResultList = new ListView<>();
	private final Popup popupGorevMusteriArama = new Popup();
	
	
	private MusteriDAO musteriDAO =  (MusteriDAO) DAOUtil.getDaoFor(Musteri.class);
	
	private final ObservableList<EmlakTuru>emlakTuruObs = FXCollections.observableArrayList(EmlakTuru.values());
	private final ObservableList<IsitmaTuru>isitmaTuruObs = FXCollections.observableArrayList(IsitmaTuru.values());
	private final ObservableList<IslemTuru>islemTuruObs = FXCollections.observableArrayList(IslemTuru.values());
	
	@FXML
	private void initialize(){
		emlakTab.setText("Emlak");

		isitmaTuruCmb.setItems(isitmaTuruObs);
		emlakTuruCmb.setItems(emlakTuruObs);
		islemTuruCmb.setItems(islemTuruObs);
		
		EventStreams.valuesOf(musteriAraTxt.textProperty()).successionEnds(Duration.ofMillis(350))
		.subscribe(e -> {
			List<Musteri> m1 =  musteriDAO.searchMusteri(musteriAraTxt.getText()); 
			if (m1 != null && m1.size() > 0) {
				musteriSearchList = FXCollections.observableList(m1);
				Point2D point = musteriAraTxt.localToScene(0.0, 0.0);
				searchResultList.setItems(musteriSearchList);
				popupGorevMusteriArama.getContent().clear();
				popupGorevMusteriArama.getContent().add(searchResultList);
				popupGorevMusteriArama.show(musteriAraTxt, point.getX(), point.getY() + 50);
			}
		});
		searchResultList.setOnMouseClicked(e -> {
			if (e.getClickCount() == 2) {
				musteri = searchResultList.getSelectionModel().getSelectedItem();
				popupGorevMusteriArama.hide();
				musteriAraTxt.clear();
				musteriAraTxt.setText(musteri.getAd() + " " + musteri.getSoyad());
				musteriAraTxt.requestFocus();
				
			}
		});
		searchResultList.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ENTER) {
				musteri = searchResultList.getSelectionModel().getSelectedItem(); 
				popupGorevMusteriArama.hide();
				musteriAraTxt.clear();
				musteriAraTxt.setText(musteri.getAd() + " " + musteri.getSoyad());
				emlakTuruCmb.requestFocus();
			}
		});
			
		emlakTuruCmb.valueProperty().bindBidirectional(emlak.turuProperty());
		metreKareTxt.textProperty().bindBidirectional(emlak.metreKareProperty());
		odaSayTxt.textProperty().bindBidirectional(emlak.odaSayisiProperty());
		salonSayTxt.textProperty().bindBidirectional(emlak.salonSayisiProperty());
		katNoTxt.textProperty().bindBidirectional(emlak.katNoProperty());
		ilTxt.textProperty().bindBidirectional(emlak.ilProperty());
		ilceTxt.textProperty().bindBidirectional(emlak.ilceProperty());
		isitmaTuruCmb.valueProperty().bindBidirectional(emlak.isitmaTuruProperty());
	}
	private void kaydet(){
		emlak.setMusteri(musteri);
		if(emlak.getMusteri()==null){
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("HATA");
			alert.setHeaderText("L�TFEN M��TER� SE��N�Z..");
			alert.show();
			musteriAraTxt.requestFocus();
		}
		else{
			if(emlak.getId()==null)
				emlak.setEmlakDurumu(EmlakDurumu.AKTIF);
		}
		DAOUtil.getDaoFor(Emlak.class).add(emlak);
		EventbusUtil.getEventBus().post(emlak);

	}
	@FXML
	private void musteriBulBtn() {
		MusteriDialog md = new MusteriDialog();
		md.showAndWait().ifPresent(ms -> emlak.setMusteri(ms));
		if (emlak.getMusteri() != null)
			musteriAraTxt.setText(emlak.getMusteri().getAd() + " " + emlak.getMusteri().getSoyad());
	}
	@FXML
	private void kaydetBtn(){
		kaydet();
	}
	@FXML
	private void kaydetKapatBtn(){
		kaydet();
		emlakTab.getTabPane().getTabs().remove(emlakTab);
	}
	@FXML
	private void kapatBtn(){
		emlakTab.getTabPane().getTabs().remove(emlakTab);
	}
}
