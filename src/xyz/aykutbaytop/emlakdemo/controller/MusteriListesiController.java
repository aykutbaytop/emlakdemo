package xyz.aykutbaytop.emlakdemo.controller;


import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import xyz.aykutbaytop.emlakdemo.dao.DAOUtil;
import xyz.aykutbaytop.emlakdemo.dao.MusteriDAO;
import xyz.aykutbaytop.emlakdemo.enums.KomutTuru;
import xyz.aykutbaytop.emlakdemo.model.Musteri;
import xyz.aykutbaytop.emlakdemo.util.EventbusUtil;
import xyz.aykutbaytop.emlakdemo.util.Komut;

public class MusteriListesiController implements Initializable {
	@FXML private Tab musteriListesiTab;
	@FXML private TextField aramaTxt;
	
	@FXML private TableView<Musteri> musteriTable;
	@FXML private TableColumn<Musteri, String> adCol;
	@FXML private TableColumn<Musteri, String> soyadCol;
	@FXML private TableColumn<Musteri, String> evTelefonCol;
	@FXML private TableColumn<Musteri, String> cepTelefonCol;
	@FXML private TableColumn<Musteri, String> ePostaCol;

	private MusteriDAO musteriDAO = (MusteriDAO) DAOUtil.getDaoFor(Musteri.class);
	private ObservableList<Musteri> musteriObs;
	
//
//	@Subscribe
//	public void	task(Musteri s){
//		if (musteriObs.contains(s)) {
//			musteriObs.remove(s);
//		}
//		musteriObs.add(s);
//	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		musteriObs = FXCollections.observableArrayList(musteriDAO.list());

		FilteredList<Musteri> filteredData = new FilteredList<>(musteriObs, e -> true);

		aramaTxt.setTooltip(new Tooltip("filtrelemeleriniz (Ad,Soyad) & (CepTel) Alanlar�na g�re Yaparbilirsiniz."));
		aramaTxt.textProperty().addListener(
				(observable, oldValue, newValue) -> {
					filteredData.setPredicate(musteri -> {

				String lowerCaseFilter = newValue.toLowerCase();

				if (newValue == null || newValue.isEmpty())
					return true;
				else if (musteri.getAd().toLowerCase().indexOf(lowerCaseFilter) != -1)
					return true;
				else if (musteri.getSoyad().toLowerCase().indexOf(lowerCaseFilter) != -1)
					return true;
				else
					return false;
			});
		});

		SortedList<Musteri> sortedData = new SortedList<>(filteredData);
		sortedData.comparatorProperty().bind(musteriTable.comparatorProperty());

		musteriTable.setItems(sortedData);

		adCol.setCellValueFactory(cellData -> cellData.getValue().adProperty());
		soyadCol.setCellValueFactory(cellData -> cellData.getValue().soyadProperty());
		evTelefonCol.setCellValueFactory(cellData -> cellData.getValue().evTelefonuProperty());
		cepTelefonCol.setCellValueFactory(cellData -> cellData.getValue().cepTelefonuProperty());
		ePostaCol.setCellValueFactory(cellData -> cellData.getValue().ePostaProperty());	
	}
	
	@FXML
	private void yeniBtn(){
		EventbusUtil.getEventBus().post(new Komut(KomutTuru.MUSTERI));
	}
	
	@FXML
	private void duzenleBtn(){
		Musteri selectMusteri = musteriTable.getSelectionModel().getSelectedItem();
		if(selectMusteri!=null)
			EventbusUtil.getEventBus().post(new Komut(KomutTuru.MUSTERI, selectMusteri));
		else{
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("HATA");
			alert.setHeaderText("M��teri se�imi yap�lmam��..");
			alert.setContentText("Tablo'dan se�im yap�n�z.");
			alert.show();
		}
	}

	@FXML
	private void silBtn() {
		Musteri a = musteriTable.getSelectionModel().getSelectedItem();
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Onay");
		alert.setHeaderText("Kay�t Silinecek. Devam Etmek �stiyormusunuz?");
		ButtonType kaydetBtn = new ButtonType("Tamam",ButtonData.OK_DONE);
		ButtonType iptalBtn = new ButtonType("�ptal", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(kaydetBtn, iptalBtn);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == kaydetBtn){
			musteriObs.remove(a);
			musteriDAO.remove(a);
		}		
	}
	
	@FXML
	private void kapatBtn(ActionEvent e){
		musteriListesiTab.getTabPane().getTabs().remove(musteriListesiTab);
	}
}