package xyz.aykutbaytop.emlakdemo.model;

import java.util.UUID;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Transient;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import xyz.aykutbaytop.emlakdemo.enums.EmlakDurumu;
import xyz.aykutbaytop.emlakdemo.enums.EmlakTuru;
import xyz.aykutbaytop.emlakdemo.enums.IsitmaTuru;
import xyz.aykutbaytop.emlakdemo.enums.IslemTuru;

@Entity(name = "EMLAK")
@Access(AccessType.PROPERTY)
public class Emlak {

	private UUID id;

	@Id
	@Column(name = "ID", columnDefinition = "CHAR(32)")
	public UUID getId() {
		return this.id;
	}

	public void setId(UUID value) {
		this.id = value;
	}
	private SimpleObjectProperty<IslemTuru> islemTuru = new SimpleObjectProperty<IslemTuru>();
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "ISLEM_TURU")
	public IslemTuru getIslemTuru() {
		return this.islemTuru.get();
	}

	public void setIslemTuru(IslemTuru value) {
		this.islemTuru.set(value);
	}

	@Transient
	public SimpleObjectProperty<IslemTuru> islemTuruProperty() {
		return islemTuru;
	}

	private SimpleObjectProperty<EmlakTuru> turu = new SimpleObjectProperty<EmlakTuru>();
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "TURU")
	public EmlakTuru getTuru() {
		return this.turu.get();
	}

	public void setTuru(EmlakTuru value) {
		this.turu.set(value);
	}

	@Transient
	public SimpleObjectProperty<EmlakTuru> turuProperty() {
		return turu;
	}
	
	private SimpleObjectProperty<IsitmaTuru> isitmaTuru = new SimpleObjectProperty<IsitmaTuru>();
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "ISITMA_TURU")
	public IsitmaTuru getIsitmaTuru() {
		return this.isitmaTuru.get();
	}
	public void setIsitmaTuru(IsitmaTuru value) {
		this.isitmaTuru.set(value);
	}
	@Transient
	public SimpleObjectProperty<IsitmaTuru> isitmaTuruProperty() {
		return isitmaTuru;
	}
	
	
	private SimpleObjectProperty<EmlakDurumu> emlakDurumu = new SimpleObjectProperty<EmlakDurumu>();
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "EMLAK_DURUMU")
	public EmlakDurumu getEmlakDurumu() {
		return this.emlakDurumu.get();
	}
	public void setEmlakDurumu(EmlakDurumu value) {
		this.emlakDurumu.set(value);
	}
	@Transient
	public SimpleObjectProperty<EmlakDurumu> emlakDurumuProperty() {
		return emlakDurumu;
	}
	
	

	private SimpleStringProperty metreKare = new SimpleStringProperty();

	@Column(name = "METRE_KARE")
	public String getMetreKare() {
		return this.metreKare.get();
	}

	public void setMetreKare(String value) {
		this.metreKare.set(value);
	}

	@Transient
	public SimpleStringProperty metreKareProperty() {
		return metreKare;
	}


	private SimpleStringProperty adres = new SimpleStringProperty();

	@Column(name = "ADRES")
	public String getAdres() {
		return this.adres.get();
	}

	public void setAdres(String value) {
		this.adres.set(value);
	}

	@Transient
	public SimpleStringProperty adresProperty() {
		return adres;
	}

	private SimpleStringProperty odaSayisi = new SimpleStringProperty();

	@Column(name = "ODA_SAYISI")
	public String getOdaSayisi() {
		return this.odaSayisi.get();
	}

	public void setOdaSayisi(String value) {
		this.odaSayisi.set(value);
	}

	@Transient
	public SimpleStringProperty odaSayisiProperty() {
		return odaSayisi;
	}

	private SimpleStringProperty salonSayisi = new SimpleStringProperty();

	@Column(name = "SALON_SAYISI")
	public String getSalonSayisi() {
		return this.salonSayisi.get();
	}

	public void setSalonSayisi(String value) {
		this.salonSayisi.set(value);
	}

	@Transient
	public SimpleStringProperty salonSayisiProperty() {
		return salonSayisi;
	}

	private SimpleStringProperty katNo = new SimpleStringProperty();

	@Column(name = "KAT_NO")
	public String getKatNo() {
		return this.katNo.get();
	}

	public void setKatNo(String value) {
		this.katNo.set(value);
	}

	@Transient
	public SimpleStringProperty katNoProperty() {
		return katNo;
	}


	private SimpleStringProperty il = new SimpleStringProperty();

	@Column(name = "IL")
	public String getIl() {
		return this.il.get();
	}

	public void setIl(String value) {
		this.il.set(value);
	}

	@Transient
	public SimpleStringProperty ilProperty() {
		return il;
	}


	private SimpleStringProperty ilce = new SimpleStringProperty();

	@Column(name = "ILCE")
	public String getIlce() {
		return this.ilce.get();
	}

	public void setIlce(String value) {
		this.ilce.set(value);
	}

	@Transient
	public SimpleStringProperty ilceProperty() {
		return ilce;
	}


	
	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.PERSIST)
	@JoinColumn(name = "MUSTERI_ID")
	
	public Musteri getMusteri() {
		return this.musteri.get();
	}

	private SimpleObjectProperty<Musteri> musteri = new SimpleObjectProperty<>();
	private boolean isMusteriSetted = false;

	public void setMusteri(Musteri value) {
		this.musteri.set(value);
		this.isMusteriSetted = true;
	}

	public SimpleObjectProperty<Musteri> musteriProperty() {
		if (!isMusteriSetted)
			getMusteri();
		return musteri;
	}
	

	@PrePersist
	private void prePersist() {
		if (id == null)
			id = UUID.randomUUID();
	}
}
