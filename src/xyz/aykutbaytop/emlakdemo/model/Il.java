package xyz.aykutbaytop.emlakdemo.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import javafx.beans.property.SimpleStringProperty;

@Entity(name ="IL")
@Access(AccessType.PROPERTY)
public class Il {
	int id;
	
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	private SimpleStringProperty tanim = new SimpleStringProperty();
	@Column(name="TANIM")
	public String getTanim(){ 
		return this.tanim.get();
	}
	
	public void setTanim(String value){
		this.tanim.set(value);
	}
	
	@Transient
	public SimpleStringProperty tanimProperty(){ 
		return tanim;}

	 @Override
	 public String toString() {
		 return getTanim();
	 }
}

