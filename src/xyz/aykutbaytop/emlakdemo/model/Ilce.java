package xyz.aykutbaytop.emlakdemo.model;

import javafx.beans.property.*;
import javax.persistence.*;

@Entity(name ="ILCE")
@Access(AccessType.PROPERTY)
public class Ilce {
	int id;
	
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	protected SimpleStringProperty tanim = new SimpleStringProperty();
	@Column(name="TANIM")
	public String getTanim(){ 
		return this.tanim.get();}
	

	public void setTanim(String value){
		this.tanim.set(value);}
	
	@Transient
	public SimpleStringProperty tanimProperty(){ 
		return tanim;}

	@Override
	public String toString() {
		return getTanim();
	}
}

