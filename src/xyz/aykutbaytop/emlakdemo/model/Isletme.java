package xyz.aykutbaytop.emlakdemo.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import javafx.beans.property.SimpleStringProperty;

@Entity(name ="ISLETME")
@Access(AccessType.PROPERTY)
public class Isletme {

	private int id;
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId(){return this.id;}
	public void setId(int value){this.id = value;}

	
	private SimpleStringProperty ad = new SimpleStringProperty();
	@Column(name="AD")
	public String getAd(){ 
		return this.ad.get();}
	

	public void setAd(String value){
		this.ad.set(value);}
	
	@Transient
	public SimpleStringProperty adProperty(){ 
		return ad;}

	private SimpleStringProperty telefonu = new SimpleStringProperty();
	@Column(name="EV_TELEFONU")
	public String getTelefonu(){ 
		return this.telefonu.get();}
	

	public void setTelefonu(String value){
		this.telefonu.set(value);}
	
	@Transient
	public SimpleStringProperty telefonuProperty(){ 
		return telefonu;}

	private SimpleStringProperty adres = new SimpleStringProperty();
	@Column(name="ADRES")
	public String getAdres(){ 
		return this.adres.get();}
	

	public void setAdres(String value){
		this.adres.set(value);}
	
	@Transient
	public SimpleStringProperty adresProperty(){ 
		return adres;}

	private SimpleStringProperty fax = new SimpleStringProperty();
	@Column(name="FAX")
	public String getFax(){ 
		return this.fax.get();}
	
	public void setFax(String value){
		this.fax.set(value);}
	
	@Transient
	public SimpleStringProperty faxProperty(){ 
		return fax;
	}
	private SimpleStringProperty yetkili = new SimpleStringProperty();
	
	@Column(name="YETKILI")
	public String getYetkili(){ 
		return this.yetkili.get();}
	

	public void setYetkili(String value){
		this.yetkili.set(value);}
	
	@Transient
	public SimpleStringProperty yetkiliProperty(){ 
		return yetkili;
	}
	
	private SimpleStringProperty ePosta = new SimpleStringProperty();
	
	@Column(name="EPOSTA")
	public String getEposta(){ 
		return this.ePosta.get();}
	

	public void setEposta(String value){
		this.ePosta.set(value);}
	
	@Transient
	public SimpleStringProperty ePostaProperty(){ 
		return ePosta;
	}

}

