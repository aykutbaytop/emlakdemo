package xyz.aykutbaytop.emlakdemo.model;

import java.util.UUID;
import java.time.*;
import java.time.format.*;
import java.util.*;
import javafx.collections.*;
import javafx.beans.property.*;
import java.beans.PropertyChangeListener;

import javax.persistence.*;
import javafx.beans.binding.*;
import java.util.stream.Collectors;

@Entity(name ="MUSTERI")
@Access(AccessType.PROPERTY)
public class Musteri {

	private UUID id;
	@Id
	@Column(name="ID", columnDefinition="CHAR(32)")
	public UUID getId(){return this.id;}
	public void setId(UUID value){this.id = value;}

	
	private SimpleStringProperty ad = new SimpleStringProperty();
	@Column(name="AD")
	public String getAd(){ 
		return this.ad.get();}
	

	public void setAd(String value){
		this.ad.set(value);}
	
	@Transient
	public SimpleStringProperty adProperty(){ 
		return ad;
	}

	private SimpleStringProperty soyad = new SimpleStringProperty();
	@Column(name="SOYAD")
	public String getSoyad(){ 
		return this.soyad.get();}
	

	public void setSoyad(String value){
		this.soyad.set(value);}
	
	@Transient
	public SimpleStringProperty soyadProperty(){ 
		return soyad;}

	private SimpleStringProperty evTelefonu = new SimpleStringProperty();
	@Column(name="EV_TELEFONU")
	public String getEvTelefonu(){ 
		return this.evTelefonu.get();}
	

	public void setEvTelefonu(String value){
		this.evTelefonu.set(value);}
	
	@Transient
	public SimpleStringProperty evTelefonuProperty(){ 
		return evTelefonu;
	}

	private SimpleStringProperty cepTelefonu = new SimpleStringProperty();
	@Column(name="CEP_TELEFONU")
	public String getCepTelefonu(){ 
		return this.cepTelefonu.get();}
	

	public void setCepTelefonu(String value){
		this.cepTelefonu.set(value);}
	
	@Transient
	public SimpleStringProperty cepTelefonuProperty(){ 
		return cepTelefonu;}


	private SimpleStringProperty ePosta = new SimpleStringProperty();
	@Column(name="EPOSTA")
	public String getEposta(){ 
		return this.ePosta.get();}
	

	public void setEposta(String value){
		this.ePosta.set(value);}
	
	@Transient
	public SimpleStringProperty ePostaProperty(){ 
		return ePosta;}
	
	private SimpleStringProperty adres = new SimpleStringProperty();
	@Column(name="ADRES")
	public String getAdres(){ 
		return this.adres.get();
	}
	

	public void setAdres(String value){
		this.adres.set(value);}
	
	@Transient
	public SimpleStringProperty adresProperty(){ 
		return adres;
	}
	
	@Transient
	public StringBinding isimSoyisimBinding = Bindings.createStringBinding(
			()->{
				if (adProperty().get()!=null)
					return adProperty().get().trim()+" "+soyadProperty().get().trim();
				else
					return "";}
			, adProperty());	

	@Override
	public String toString() {
		if(getAd() != null && getSoyad()!=null)
			return (getAd() + " " + getSoyad()).trim();
		else if (getAd() != null)
			return getAd().trim();
		else if (getSoyad() != null)
			return getSoyad().trim();
		return "";
	}

	@PrePersist
	private void prePersist(){
		if (id == null) id = UUID.randomUUID();
	}

}

